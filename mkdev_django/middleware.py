from mkdev_django.user_agent_list import user_agent_list


class MobileTemplatesMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user_agent = request.META.get('HTTP_USER_AGENT')
        request.mobile = user_agent in user_agent_list
        response = self.get_response(request)
        return response
