from django.contrib import sitemaps
from django.urls import reverse
from newsfeed.models import NewsPost, Article


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['newsfeed:news-list',
                'newsfeed:article-list',
                ]

    def location(self, item):
        return reverse(item)


class NewsSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return NewsPost.objects.all()


class ArticlesSitemap(sitemaps.Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Article.objects.all()
