from django.contrib import admin
from django.urls import path, include
from django.contrib.flatpages import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap, NewsSitemap, ArticlesSitemap
from newsfeed.urls import *
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.flatpages.sitemaps import FlatPageSitemap
from rest_framework.routers import DefaultRouter
from newsfeed import views


sitemaps = {
    'static': StaticViewSitemap,
    'flatpages': FlatPageSitemap,
    'news': NewsSitemap,
    'articles': ArticlesSitemap,
}

router = DefaultRouter()
router.register('api/newsfeed', views.NewsViewSet)
router.register('api/articles', views.ArticlesViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('accounts.urls')),
    path('', include('newsfeed.urls', namespace='newsfeed')),
    path('', TemplateView.as_view(template_name="main_page.html")),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('', include(router.urls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

urlpatterns += path('', include('django.contrib.flatpages.urls')),  # в конце, потому что любит перехватывать ссылки
