from django.db import models
from django.contrib.flatpages.models import FlatPage
from meta.models import ModelMeta


class FlatPageMeta(ModelMeta, models.Model):
    flatpage = models.OneToOneField(FlatPage, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    keywords = models.CharField(max_length=200)
