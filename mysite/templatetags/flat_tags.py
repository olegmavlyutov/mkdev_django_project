from django import template
from mysite.models import FlatPageMeta


register = template.Library()


@register.inclusion_tag('flatpages/flat_meta.html')
def load_flat_meta(flat_id):
    queryset = FlatPageMeta.objects.get(flatpage__id=flat_id)
    return {'tags_list': queryset}
