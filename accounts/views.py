from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.edit import UpdateView
from .forms import CustomUserChangeForm, CustomUserCreationForm


class SignUpView(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class ProfileUpdate(UpdateView):
    form_class = CustomUserChangeForm
    success_url = reverse_lazy('profile')

    def get_object(self, queryset=None):
        return self.request.user
