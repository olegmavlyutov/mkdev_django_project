# Generated by Django 2.2.3 on 2019-08-26 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsfeed', '0012_auto_20190826_1727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newspost',
            name='image',
            field=models.ImageField(default='default.jpg', upload_to='uploads'),
        ),
    ]
