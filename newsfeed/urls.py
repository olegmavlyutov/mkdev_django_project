from django.urls import path, re_path
from newsfeed.views import *

app_name = 'newsfeed'

urlpatterns = [
    path(r'newsfeed/<slug:slug>/', NewsPostDetailView.as_view(), name='news-detail'),
    path(r'newsfeed/', NewsPostListView.as_view(), name='news-list'),
    path(r'articles/<slug:slug>/', ArticleDetailView.as_view(), name='article-detail'),
    path(r'articles/', ArticleListView.as_view(), name='article-list'),
    path(r'search/', SearchListView.as_view(), name='search'),
    path(r'archive/', zip_archive, name='archive-csv'),
]
