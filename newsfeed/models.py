from django.db import models
from django.conf import settings
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.mail import send_mail
from django.contrib.auth.models import Group
from accounts.models import CustomUser
from .utils import get_unique_slug
from ckeditor.fields import RichTextField
from django.urls import reverse
from django.contrib.sitemaps import ping_google
from meta.models import ModelMeta


STATUS_CHOICES = [
    ('d', 'Draft'),
    ('p', 'Published'),
    ('w', 'Withdrawn'),
]


class NewsPost(ModelMeta, models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )
    image = models.ImageField(
        upload_to='uploads',
        height_field=None,
        width_field=None,
        max_length=100,
        default='default.jpg'
    )
    text = models.TextField(max_length=200)
    slug = models.SlugField(max_length=300, default='', unique=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='d')
    _metadata = {
        'keywords': 'title',
        'description': 'title',
        'image': 'get_meta_image',
    }

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_unique_slug(self, 'title', 'slug')
        super().save(*args, **kwargs)
        ping_google()

    # def get_absolute_url(self):
    #     return reverse('news-detail', args=[str(self.slug)])

    def get_absolute_url(self):
        return "/newsfeed/%i/" % self.id

    def get_meta_image(self):
        if self.image:
            return self.image.url


class Category(models.Model):
    title = models.CharField(max_length=100)


class Article(ModelMeta, models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    tags = models.ManyToManyField(
        'Tag',
        related_name="tags",
        blank=True,
    )
    image = models.ImageField(
        upload_to='uploads',
        height_field=None,
        width_field=None,
        max_length=100,
        default='default.jpg')
    content = RichTextField(default='')
    slug = models.SlugField(max_length=300, default='', unique=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='d')
    _metadata = {
        'keywords': 'title',
        'description': 'title',
        'image': 'get_meta_image',
    }

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_unique_slug(self, 'title', 'slug')
        super().save(*args, **kwargs)
        ping_google()

    # def get_absolute_url(self):
    #     return reverse('article-detail', args=[str(self.slug)])

    def get_absolute_url(self):
        return "/articles/%i/" % self.id

    def get_meta_image(self):
        if self.image:
            return self.image.url


class Tag(models.Model):
    title = models.CharField(max_length=100)


@receiver(post_save, sender=NewsPost)
def send_notifications(sender, instance, *args, **kwargs):
    if instance.published_date:
        group = Group.objects.get(name='mailing_group')
        send_mail('Опубликована новость на сайте',
                  'Зайдите на сайт - там опубликована новость',
                  'web-django-test@yandex.ru',
                  group.user_set.values_list('email', flat=True), fail_silently=False)
