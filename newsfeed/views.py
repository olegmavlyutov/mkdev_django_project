from django.shortcuts import render
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from newsfeed.models import NewsPost, Article
from django.contrib.postgres.search import SearchVector, SearchQuery
from itertools import chain
import csv
from django.http import HttpResponse
import zipfile
import os
from io import BytesIO
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import serializers
from django_filters.rest_framework import DjangoFilterBackend


class NewsPostDetailView(DetailView):

    model = NewsPost

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['meta'] = self.get_object().as_meta(self.request)
        return context


class NewsPostListView(ListView):

    model = NewsPost
    queryset = NewsPost.objects.filter(published_date__isnull=False)
    template_name = 'newsfeed/newspost_list.html'
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        news_posts = NewsPost.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
        if request.mobile:
            return render(request, 'mobile/newsfeed/newspost_list.html', {'news_list': news_posts})
        else:
            return render(request, 'newsfeed/newspost_list.html', {'news_list': news_posts})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

    def get_queryset(self):
        news_queryset = super().get_queryset()
        d = self.request.GET.copy()
        d.pop('page', 'default_key')
        return news_queryset.filter(**d)


class ArticleDetailView(DetailView):

    model = Article

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['meta'] = self.get_object().as_meta(self.request)
        return context


class ArticleListView(ListView):

    model = Article
    queryset = Article.objects.filter(published_date__isnull=False)
    template_name = 'newsfeed/article_list.html'
    paginate_by = 10

    def get(self, request, *args, **kwargs):
        articles = Article.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
        if request.mobile:
            return render(request, 'mobile/newsfeed/article_list.html', {'article_list': articles})
        else:
            return render(request, 'newsfeed/article_list.html', {'article_list': articles})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

    def get_queryset(self):
        articles_queryset = super().get_queryset()
        d = self.request.GET.copy()
        d.pop('page', 'default_key')
        return articles_queryset.filter(**d)


class SearchListView(ListView):
    model = NewsPost, Article
    paginate_by = 10
    template_name = 'newsfeed/search_list.html'

    def get(self, request, *args, **kwargs):
        search_list = self.get_queryset()
        if request.mobile:
            return render(request, 'mobile/newsfeed/search_list.html', {'search_list': search_list})
        else:
            return render(request, 'newsfeed/search_list.html', {'search_list': search_list})

    def get_queryset(self):
        queryset_news = NewsPost.objects.filter(published_date__isnull=False)
        queryset_articles = Article.objects.filter(published_date__isnull=False)

        keywords = self.request.GET.get('q')
        query = SearchQuery(keywords)
        news_title_vector = SearchVector('title', weight='A')
        news_content_vector = SearchVector('text', weight='B')
        news_vectors = news_title_vector + news_content_vector
        queryset_news = queryset_news.annotate(search=news_vectors).filter(search=query)

        article_title_vector = SearchVector('title', weight='A')
        article_content_vector = SearchVector('content', weight='B')
        article_vectors = article_title_vector + article_content_vector
        queryset_articles = queryset_articles.annotate(search=article_vectors).filter(search=query)

        return list(chain(queryset_articles, queryset_news))


def article_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="articles.csv"'
    articles = Article.objects.filter(published_date__isnull=False)
    writer = csv.writer(response)
    for article in articles:
        writer.writerow([article.title, article.content, article.created_date])
    return response


def news_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="newsfeed.csv"'
    news_list = NewsPost.objects.filter(published_date__isnull=False)
    writer = csv.writer(response)
    for news in news_list:
        writer.writerow([news.title, news.text, news.created_date])
    return response


def zip_archive(request):
    with open('articles.csv', 'w', newline='') as article_file:
        article_fields = ['Title', 'Text', 'Created date']
        articles = Article.objects.filter(published_date__isnull=False)
        article_writer = csv.DictWriter(article_file, fieldnames=article_fields, delimiter=';')
        article_writer.writeheader()
        for article in articles:
            article_writer.writerow({
                'Title': article.title,
                'Text': article.content,
                'Created date': article.created_date
            })

    with open('newsfeed.csv', 'w', newline='') as news_file:
        news_fields = ['Title', 'Text', 'Created date']
        news_list = NewsPost.objects.filter(published_date__isnull=False)
        news_writer = csv.DictWriter(news_file, fieldnames=news_fields, delimiter=';')
        news_writer.writeheader()
        for news in news_list:
            news_writer.writerow({
                'Title': news.title,
                'Text': news.text,
                'Created date': news.created_date
            })

    buffer = BytesIO()
    with zipfile.ZipFile(buffer, 'w') as archive:
        archive.write('articles.csv')
        archive.write('newsfeed.csv')

    os.remove('articles.csv')
    os.remove('newsfeed.csv')

    response = HttpResponse(buffer.getvalue())
    response['Content-Type'] = 'application/x-zip-compressed'
    response['Content-Disposition'] = 'attachment; filename=archive.zip'
    return response


class NewsListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='newspost-detail', read_only=True)
    author = serializers.StringRelatedField(many=False)

    class Meta:
        model = NewsPost
        fields = ['id', 'author', 'title', 'text', 'image', 'url']


class NewsViewSet(viewsets.ModelViewSet):
    queryset = NewsPost.objects.filter(published_date__isnull=False)
    serializer_class = NewsListSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['category__title', 'author__username']

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class ArticleListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='article-detail', read_only=True)
    author = serializers.StringRelatedField(many=False)

    class Meta:
        model = Article
        fields = ['id', 'author', 'title', 'content', 'image', 'url']


class ArticlesViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.filter(published_date__isnull=False)
    serializer_class = ArticleListSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['tags__title', 'author__username']

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


# class CategoryListView(ListView):
#
#     model = NewsPost
#     paginate_by = 100
#     queryset = NewsPost.objects.filter(published_date__isnull=False)
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['now'] = timezone.now()
#         return context
#
#     def news_list(request):
#         news_posts = NewsPost.objects.filter(category_title='javascript')
#         return render(request, 'post_list.html', {'news_posts': news_posts})
