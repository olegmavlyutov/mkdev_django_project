from django import template
from newsfeed.views import *
import random

register = template.Library()


@register.inclusion_tag('newsfeed/news_block.html')
def load_news(count):
    queryset = NewsPost.objects.all().order_by('-published_date')[:count]
    return {'object_list': queryset}


@register.inclusion_tag('newsfeed/featured_news_block.html')
def load_featured_news(category_id):
    id_list = NewsPost.objects.filter(
        published_date__lte=timezone.now(),
        category__id__contains=category_id).values_list('id', flat=True)
    new_list = []
    i = 0
    while len(new_list) < 5:
        new_list.append(random.choice(id_list))
        i += 1
    queryset = NewsPost.objects.filter(id__in=new_list).order_by('-published_date')[:3]
    return {'object_list': queryset}
