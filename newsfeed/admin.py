from datetime import datetime
from django.contrib import admin
from .models import NewsPost, Category, Article, Tag


class NewsPostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'category', 'slug', 'status', 'published_date']
    ordering = ['title']
    actions = ['make_published']

    def make_published(self, request, queryset):
        queryset.update(status='p', published_date=datetime.now())
    make_published.short_description = "Mark selected news as published"


class ArticlePostAdmin(admin.ModelAdmin):
    list_display = ['title', 'author', 'slug', 'status', 'published_date']
    ordering = ['title']
    actions = ['make_published']

    def make_published(self, request, queryset):
        queryset.update(status='p', published_date=datetime.now())
    make_published.short_description = "Mark selected articles as published"


admin.site.register(NewsPost, NewsPostAdmin)
admin.site.register(Category)
admin.site.register(Article, ArticlePostAdmin)
admin.site.register(Tag)
