from django.core.management.base import BaseCommand
from django_fakery import factory


class Command(BaseCommand):
    help = 'Create random posts'

    def add_arguments(self, parser):
        parser.add_argument('--count', type=int, help='Number of posts to be created')
        parser.add_argument('--model', help='app_label.ModelName')

    def handle(self, *args, **kwargs):
        instance = factory.blueprint(kwargs['model'])
        instance.make(quantity=int(kwargs['count']))
        
        self.stdout.write(self.style.SUCCESS('Successfully created post "%s"' % kwargs['count']))
