from django.test import TestCase
from .models import NewsPost
from accounts.models import CustomUser
from django.contrib.auth.models import Group
from django.utils import timezone


class NewsPostTestCase(TestCase):
    def setUp(self):
        CustomUser.objects.create_user(
            username='oleg',
            password='1qazXSW@',
        )
        Group.objects.create(
            name='mailing_group',
        )
        NewsPost.objects.create(
            author=CustomUser.objects.get(username='oleg'),
            title='some_title',
            text='some_text',
            published_date=timezone.now(),
            status='p',
        )

    def test_browser_respond(self):
        response = self.client.get('/newsfeed/')
        print('Response status code: ' + str(response.status_code))
        post = NewsPost.objects.get(author=CustomUser.objects.get(username='oleg'))
        print('Created post by author with title: ' + str(post))
